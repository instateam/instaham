package com.example.domi.myapplication2;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.domi.myapplication2.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RegisterActivity extends Activity {

    Button resigerButton;
    EditText username, password, password2;
    private DBLayer dblayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Creating DBLayer
        dblayer = new DBLayer(this);

        this.initInstances();
    }

    private void initInstances(){
        //Instanzen aus XMl bekannt machen
        this.resigerButton = (Button) findViewById(R.id.registerButton);
        this.username = (EditText) findViewById(R.id.usernameValueregister);
        this.password = (EditText) findViewById(R.id.passwordValueregister);
        this.password2 = (EditText) findViewById(R.id.passwordValueRepeat);
    }

    public void onRegisterDoneButtonClicked(View v){
        if(this.password.getText().toString().equals(this.password2.getText().toString())){
            String result = dblayer.setDataRegister(this.username.getText().toString(), this.password.getText().toString());

            if(result == "Databaseerror"){
                Toast.makeText(this, "Couldnt connect to database", Toast.LENGTH_LONG).show();
            }
            else if(result == "Error"){
                Toast.makeText(this, "Etwas lief schief :(", Toast.LENGTH_LONG).show();
                return;
            }
            else if(result == "Usererror"){
                Toast.makeText(this, "Der Benutzer existiert bereits.", Toast.LENGTH_LONG).show();
                return;
            }
            else if(result == "finish"){
                Toast.makeText(this, "Sie haben sich erfolgreich Registriert <3.", Toast.LENGTH_LONG).show();
                this.finish();
            }
        }
        else{
            Toast.makeText(this, "Passwörter stimmmen nicht überein!", Toast.LENGTH_LONG).show();
        }
    }



}
