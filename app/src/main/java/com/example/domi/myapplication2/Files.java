package com.example.domi.myapplication2;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Steven Laptop on 03.04.2015.
 */
public class Files {

    //SESSION FILE
    public static final String sessionstr = "com.example.Instaham.session"; //Gibt den Dateipfad an

    public static String GetSessionFile(Activity caller){
        SharedPreferences filesession = caller.getSharedPreferences("test", Context.MODE_PRIVATE);
        return filesession.getString(sessionstr, "Gast");
    }

    public static void SetSessionFile(Activity caller, String session){
        SharedPreferences filesession = caller.getApplicationContext().getSharedPreferences("test", Context.MODE_PRIVATE);
        filesession.edit().putString(sessionstr,session).apply();
    }

    //SERVERPATHFILE
    public static final String serverpathstr = "com.example.Instaham.serverpath"; //Gibt den Dateipfad an

    public static String GetServerpathFile(Activity caller){
        SharedPreferences fileserverpath = caller.getSharedPreferences("test", Context.MODE_PRIVATE);
        return fileserverpath.getString(serverpathstr, "http://topruf.synology.me/Instaham");
    }

    public static void SetServerpathFile(Activity caller, String path){
        SharedPreferences fileserverpath = caller.getApplicationContext().getSharedPreferences("test", Context.MODE_PRIVATE);
        fileserverpath.edit().putString(serverpathstr,path).apply();
    }
}
