package com.example.domi.myapplication2;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MyActivity extends ActionBarActivity implements AdapterView.OnItemClickListener, ActionBar.TabListener {

    //============= Daten ===============

    DBLayer db = null;
    ArrayList<Integer> selectedFilterItems; //Indexe, der vom User gewählten zu filternden Kategorien

    //============= /Daten ===============

    //============= Oberfläche ===============

        //für Swipe-Feature
        private ViewPager viewPager;
        private ActionBar actionBar;

        //für NavigationBar
        private DrawerLayout drawerLayout;
        private ListView listView;
        private NavigationBarAdapter NavBarAdapter;

        //weitere Instanzvariablen
        private static final int MAXFRAGS = 5;

    //============= /Oberfläche ===============

    protected void onCreate(Bundle arg0){

        super.onCreate(arg0);
        setContentView(R.layout.activity_my);

        //Datenbankklasse anlegen für Netzwerkkommunikation
        this.db = new DBLayer(this);

        //List für ausgewählte Filterelemente anlegen
        this.selectedFilterItems = new ArrayList<Integer>();

        //Titel deaktivieren in ActionBar und Tabs anlegen
        this.actionBar = getActionBar();
        this.actionBar.setDisplayShowTitleEnabled(false);
        this.actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        //Tabs hinzufügen
        this.addTabsToActionBar();

        //Bekanntmachen der Layout Elemente
        this.viewPager = (ViewPager) findViewById(R.id.pager);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        this.listView = (ListView) findViewById(R.id.drawerList);

        //Adapter bekannt machen, der angibt, welche Seiten geladen werden sollen,
        //wenn der User zur nächsten Seite swiped
        this.viewPager.setAdapter(new MyAdapter(getSupportFragmentManager(), this.MAXFRAGS));

        //Adapter für Navigationbar
        this.listView.setOnItemClickListener((android.widget.AdapterView.OnItemClickListener) this);
        this.NavBarAdapter = new NavigationBarAdapter(this);
        this.listView.setAdapter(NavBarAdapter);

        //Listener des viewPagers definieren
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            //hier stehen die Events, die durch die Swipebewegung ausgelöst werden
            //(nicht durch Tabs in der oberen Leiste!)
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                Log.d("DOMI", "position: "+position+" offset: "+positionOffset);
            }

            public void onPageSelected(int position) {
                //ausgelöst, wenn User swiped zum nächsten Tab (nicht anklicken in actionBar!),
                //setze Tab in actionBar
                actionBar.setSelectedNavigationItem(position); //benötigt für Tabs (s. unten)
            }

            public void onPageScrollStateChanged(int state) {

            }
        });

        //Login aufrufen
        Intent inte = new Intent(this, LoginActivity.class);
        startActivity(inte);
    }

    //TabListener für Tableiste
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (this.viewPager != null && tab != null) {
            this.viewPager.setCurrentItem(tab.getPosition());
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    //notwendig, um Icons in ActionBar anzuzeigen
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    //überprüft, welche Kategorien vom User gefiltert wurden und gibt die
    //entsprechenden Kategorien als String-Liste zurück
    private ArrayList parseSelectedFilterItems(){

        Resources res = getResources();
        final String[] allCategories = res.getStringArray(R.array.categories);

       ArrayList<String> filteredCategories = new ArrayList<String>();

        Log.i("Filter:", "=========================");

       //überprüfen, welche Kateogiren ausgewählt wurden
       for (int i = 0; i < this.selectedFilterItems.size(); i++){
           filteredCategories.add(allCategories[this.selectedFilterItems.get(i)]);


           Log.i("Filter:", "Du hast ausgewählt:"+filteredCategories.get(i)+" Index: "+selectedFilterItems.get(i).toString());
       }

       Log.i("Filter:", "=========================");

        return filteredCategories;
    }

    //MENÜ-FUNKTIONEN (ACTION-BAR-FUNKTIONEN)
    //==============================================================================================

    //Tabs anlegen
    private void addTabsToActionBar(){

        for (int i = 0; i < this.MAXFRAGS; i ++) {
            ActionBar.Tab tab = this.actionBar.newTab();


            //vorübergehende icons hinzufügen
            switch (i) {
                case 0:
                    tab.setIcon(getResources().getDrawable( R.drawable.hauptspeise));
                    tab.setText("Hauptspeise");
                    break;
                case 1:
                    tab.setIcon(getResources().getDrawable( R.drawable.nachspeise));
                    tab.setText("Nachspeise");
                    break;
                case 2:
                    tab.setIcon(getResources().getDrawable( R.drawable.vorspeise));
                    tab.setText("Vorspeise");
                    break;
                case 3:
                    tab.setIcon(getResources().getDrawable( R.drawable.beilage));
                    tab.setText("Beilage");
                    break;
                case 4:
                    tab.setIcon(getResources().getDrawable( R.drawable.getraenk));
                    tab.setText("Getränk");
                    break;
            }
            tab.setTabListener(this);

            this.actionBar.addTab(tab);
        }
    }

    //wird aufgerufen, wenn User ein Menuelement antippt
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch(id){

            case R.id.action_refresh:
                //lade zufällige Rezepte aus Datenbank
                Toast.makeText(this, "Lade zufällige Rezepte...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_filter:
                this.ActionBarShowFilterOptions();
                break;
            case R.id.action_search:
                //öffne Eingabefenster
                this.ActionBarShowSearchDialog();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    //zeigt Input Dialog fürs Suchen
    public void ActionBarShowSearchDialog(){

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Schnelle Suche");
        alert.setMessage("Was suchst du? ");

        // Textfeld hinzufügen
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //Suchstring erhalten
                String value = input.getText().toString();

                //entsprechende Rezepte von DB anfordern
                db.getReceiptsBySearchString(value, -1);

                //Activity starten, die Rezepte in Tabellenform anzeigt
                Intent in = new Intent(getBaseContext(), SearchResults.class);
                startActivity(in);
            }
        });

        alert.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled
            }
        });

        alert.show();
    }

    //zeigt Filteroptionen in Dialogbox an, die man auswählen kann
    public void ActionBarShowFilterOptions(){
        //erhalte Tags aus strings.xml
        Resources res = getResources();
        final String[] tags = res.getStringArray(R.array.categories);

        //Dialogbox erstellen
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Wähle deine Filteroptionen aus: ");

        //zuvor ausgewählte Elemente erhalten und übergeben
        boolean[] sel = new boolean[tags.length];

        for(int i = 0; i < this.selectedFilterItems.size(); i++){
           sel[selectedFilterItems.get(i)] = true;
        }

        builder.setMultiChoiceItems(tags, sel,
                new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        if (isChecked) {
                            // Wenn User ein Element ausgewählt hat, merken:
                            selectedFilterItems.add(indexSelected);

                        } else if (selectedFilterItems.contains(indexSelected)) {
                            // Wenn Kategorie bereits ausgewählt war, wieder löschen:
                            selectedFilterItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                })// Buttons anlegen
                .setPositiveButton("Laden", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        //lade Rezepte von Datenbank hier
                       db.getReceiptsByCategory(parseSelectedFilterItems(), -1);

                       //Activity starten, die Rezepte in Tabellenform anzeigt
                        Intent in = new Intent(getBaseContext(), SearchResults.class);
                        startActivity(in);

                        //TODO:
                        //Rezepte übergeben

                    }
                })
                .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Fenster schließen
                        //do nothing
                    }
                });

        dialog = builder.create();
        dialog.show();
    }

    //==============================================================================================

    //Klick Methode für NavigationBar
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //in Variable i steht angeklickter Index

        switch (i) {
            case 0: //Erweiterte Suche
                break;
            case 1: //Rezept hochladen
                Intent in1 = new Intent(this, AddRecept.class);
                startActivity(in1);
                break;
            case 2: //Einstellungen
                break;
            case 3: //Mein Profil
                Intent in3 = new Intent(this, Myprofile.class);
                startActivity(in3);
                break;
            case 4: //Ausloggen
                Files.SetSessionFile(this, "Gast");
                Intent in4 = new Intent(this, LoginActivity.class);
                startActivity(in4);
                break;

        }
    }

}

//ADAPTERKLASSEN
//==================================================================================================

//Adapterklasse, die angibt, welche Seiten geladen werden sollen,
//wenn der User zur nächsten Seite swiped
class MyAdapter extends FragmentPagerAdapter {

    private int amount = 0;

    public MyAdapter(FragmentManager fm, int amount) {

        super(fm);
        this.amount = amount;
    }

    public Fragment getItem(int position) {

        Fragment fragment = new ReceptUI();

        return fragment;
    }

    public int getCount() {
        return this.amount;
    }
}

//Adapter, der die Zeilen mit Icons für die Navigationbar aus XMl läd
class NavigationBarAdapter extends BaseAdapter {

    private Context context;

    String[] NavItems;
    int[] images = {R.drawable.ic_action_search, R.drawable.ic_action_upload, R.drawable.ic_action_settings, R.drawable.ic_action_person, R.drawable.ic_action_forward};

    public NavigationBarAdapter(Context context) {

        this.context = context;
        this.NavItems = context.getResources().getStringArray(R.array.drawer);

    }

    @Override
    public int getCount() {
        return this.NavItems.length;
    }

    @Override
    public Object getItem(int i) {
        return this.NavItems[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = null;
        if (view == null) {
            //"konvertierte" XML in Java Objekt
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            row = inflater.inflate(R.layout.custom_row, viewGroup, false);

        }
        else {
            row = view;
        }

        //Zeilen bekannt machen
        TextView titleTextView = (TextView) row.findViewById(R.id.row_title);
        ImageView titleImageView = (ImageView) row.findViewById(R.id.row_image);
        //mit Inhalt füllen
        titleTextView.setText(NavItems[i]);
        titleImageView.setImageResource(images[i]);

        return row;
    }
}

//==================================================================================================
