package com.example.domi.myapplication2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//=================================================
//Rezeptupload Netcode
//=================================================
public class UploadToServer extends AsyncTask<Void, Void, String> {

    private Activity callingactivity;
    private String uploadresult;
    private ProgressDialog pd;
    private String title;
    private String difficulty;
    private String timetocook;
    private String author;
    private String kathegorie;
    private String shortdescription;
    private String ingredients;
    private String description;
    private String ba1;
    private String ts;

    public static String url = "http://topruf.synology.me/Instaham";

    //Construktor
    public UploadToServer(Activity caller, String tmp_titel, String tmp_difficulty, String tmp_timetocook, String tmp_author, String tmp_kathegorie, String tmp_shortdescription, String tmp_ingredients, String tmp_description, String tmp_ba1){
        String urlpath = Files.GetServerpathFile(caller);

        if(urlpath != ""){
            this.url = urlpath;
        }

        callingactivity = caller;
        pd = new ProgressDialog(caller);
        this.title = tmp_titel;
        this.difficulty = tmp_difficulty;
        this.timetocook = tmp_timetocook;
        this.author = tmp_author;
        this.kathegorie = tmp_kathegorie;
        this.shortdescription = tmp_shortdescription;
        this.ingredients = tmp_ingredients;
        this.description = tmp_description;
        this.ba1 = tmp_ba1;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ts = df.format(c.getTime());
    }

    //Befor the Class gets execute
    protected void onPreExecute() {
        super.onPreExecute();
        pd.setMessage("Rezept wird hochgeladen...!");
        pd.show();
    }

    @Override
    protected String doInBackground(Void... params) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("titel", this.title));
        nameValuePairs.add(new BasicNameValuePair("schwirigkeit", this.difficulty));
        nameValuePairs.add(new BasicNameValuePair("zeitaufwand", this.timetocook));
        nameValuePairs.add(new BasicNameValuePair("autor", this.author));
        nameValuePairs.add(new BasicNameValuePair("veroeffentlichungsdatum",this.ts));
        nameValuePairs.add(new BasicNameValuePair("kategorie",this.kathegorie));
        nameValuePairs.add(new BasicNameValuePair("kurzbeschr",this.shortdescription));
        nameValuePairs.add(new BasicNameValuePair("zutaten",this.ingredients));
        nameValuePairs.add(new BasicNameValuePair("zubereitungsbeschr",this.description));
        if(ba1 != "404"){
            nameValuePairs.add(new BasicNameValuePair("base64", this.ba1));
            nameValuePairs.add(new BasicNameValuePair("ImageName","Rezeptbilder/" + System.currentTimeMillis() + ".jpg"));
        }
        else{
            nameValuePairs.add(new BasicNameValuePair("ImageName", "Rezeptbilder/404Error.jpg"));
        }
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url + "/rezeptupload.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            String st = EntityUtils.toString(response.getEntity());
            Log.v("log_tag", "In the try Loop" + st);
            uploadresult = st;

        } catch (Exception e) {
            Log.v("log_tag", "Error in http connection " + e.toString());
        }
        return "Success";

    }

    //After the Class got execute
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        pd.hide();
        pd.dismiss();
        if(uploadresult.contains("true")){
            Toast.makeText(callingactivity, "Rezept wurde erfolgreich hochgeladen!", Toast.LENGTH_LONG).show();
            callingactivity.finish();
        }
        else if(uploadresult.contains("false")){
            Toast.makeText(callingactivity, "Es ist ein Fehler aufgetreten!", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(callingactivity, uploadresult, Toast.LENGTH_LONG).show();
        }
    }
}
