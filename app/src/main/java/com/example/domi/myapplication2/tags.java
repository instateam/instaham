package com.example.domi.myapplication2;
/**
 * öffentliche Enums für die Kategorien eines Rezepts
 */

public enum tags {HAUPTSPEISE, VORSPEISE, NACHSPEISE, BEILAGE,
    HERZHAFT, DEFTIG, SÜSS,
    MEDITERAN, EXOTISCH, ORIENTALISCH,
    LEICHT, STOPFT,
    VEGETARISCH, VEGAN, FLEISCHPUR,
    FORMIDABEL}; //bin für ergänzungen stets offen meine freudigen mitentwickler!