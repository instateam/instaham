package com.example.domi.myapplication2;

/**
 * Created by Domi on 30.07.2014.
 * Klasse, die einen Kommentar repräsentiert
 */
public class Comment {

    private String author;
    private String comment;

    public Comment(String author, int rating, String comment) {

        this.author = author;
        this.comment = comment;
    }
    //getter

    public String getComment() {
        return comment;
    }

}
