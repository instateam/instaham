package com.example.domi.myapplication2;
/**
 * öffentlicher Enum für die Schwierigkeit des Rezepts
 */
public enum difficulty {EINFACH, MITTEL, SCHWER};