package com.example.domi.myapplication2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Domi on 31.07.2014.
 */
public class DBLayer {

    public static String url = "http://topruf.synology.me/Instaham";

    public DBLayer (Activity caller){
        //Reads the UrlFile
        String urlpath = Files.GetServerpathFile(caller);

        if(urlpath != ""){
            this.url = urlpath;
        }
    }

    public DBLayer(String url){
        this.url = url;
    }

    //=================================================
    //Netzcode zum Laden der Rezepte
    //=================================================

    //läd Rezepte, die den übergebenen Kategorien entsprechen
    //in 'filteredCategories' stehen die Kategorien als Strings
    //z.B. "Hauptspeise", "Nachspeise", ...
    //Diese können an PHP übergeben werden, um dort die SQL-Querys zu basteln
    public List<Recept> getReceiptsByCategory(ArrayList filteredCategories, int amount){

        ArrayList<Recept> results = new ArrayList<Recept>();

        if (amount == -1){
            //kein Limit: Alle verfügbaren Rezepte laden!
        }

        //TODO:
        //dein Part, Steve! :-) siehe Beschreibung oben!

        return results;
    }

    //läd Rezepte, die das Suchwort "needle" enthalten
    public List<Recept> getReceiptsBySearchString(String needle, int amount){

        ArrayList<Recept> results = new ArrayList<Recept>();

        if (amount == -1){
            //kein Limit: Alle verfügbaren Rezepte laden!
        }

        return results;
    }

    //lade zufälliges Rezept aus der Datenbank
    public List<Recept> getRandomReceipts(int amount){

        ArrayList<Recept> results = new ArrayList<Recept>();

        //TODO:
        //mache an dieser Stelle ein Rezept mit der Klasse Recept.class daraus

        return results;
    }

    //=================================================
    //Login Netcode
    //=================================================
    public String getLoginData(String username, String password) {

        String result = "";
        InputStream isr = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(this.url+"/login.php?username=" + username + "&password=" + password); //YOUR PHP SCRIPT ADDRESS
            Log.i("Netzwerk", httppost.getURI().toString());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            isr = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
            return ("Databaseerror");
        }
        //convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(isr, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            isr.close();

            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error  converting result " + e.toString());
        }

        //parse json data
        try {
            if (result.contains("true")) {
                return ("Right Login");
            } else {
                return ("Wrong Login");
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("log_tag", "Error Parsing Data " + e.toString());
        }
        return ("Error");
    }


    //=================================================
    //Register Netcode
    //=================================================
    public String setDataRegister(String username, String password) {

        String result = "";
        InputStream isr = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url + "/register.php?username=" + username + "&password=" + password); //YOUR PHP SCRIPT ADDRESS
            Log.i("Netzwerk", httppost.getURI().toString());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            isr = entity.getContent();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
            return "Databaseerror";
        }
        //convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(isr, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            isr.close();

            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error  converting result " + e.toString());
        }

        //parse json data
        try {
            if(result.contains("true")) {
                return "finish";
            }
            else if(result.contains("false")){
                return "Error";
            }
            else if(result.contains("user allready exists")){
                return "Usererror";
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("log_tag", "Error Parsing Data " + e.toString());
        }
        return "Error";
    }
}


