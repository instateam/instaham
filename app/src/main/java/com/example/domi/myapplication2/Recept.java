package com.example.domi.myapplication2;

import java.text.DateFormat;
import java.util.List;

/**
 * Prototyp der Rezeptklasse, die alle Eigenschaften und Methoden eines einzelnen Rezeptes enthält.
 */

public class Recept {

    //private instances
    private int id = 0;
    private String topic = "";
    private String author = "";
    private DateFormat date;
    private String hint = "";
    private String ingredients = "";
    private String fullDescription = "";
    private double rating = 0.0;
    private int timeEffort = 0;
    private int difficulty;
    private String category;
    private List<Comment> comments;     //Liste aller Kommentare dieses Rezepts(s. Klasse Comment)

    //constructor
    public Recept(int id, String topic, String author, String hint, String ingredients, String fullDescription, int timeEffort, int difficulty, String category) {

        this.id = id;

        this.topic = topic;
        this.author = author;
        this.hint = hint;
        this.ingredients = ingredients;
        this.fullDescription = fullDescription;
        this.timeEffort = timeEffort;
        this.difficulty = difficulty;
        this.category = category;

        //Datum einfügen
        this.date = DateFormat.getDateInstance(DateFormat.SHORT);

        //Bewertungen werden dann über Kommentare eingefügt...
    }

    //methods

    /**Hinzufügen eines Kommentares an das List Objekt*/
    public void addComment(Comment comment){
        this.comments.add(comment);
    }

    //setter, getter

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}