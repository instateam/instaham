package com.example.domi.myapplication2;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.SharedPreferences;

import android.app.Activity;
import android.os.StrictMode;
import android.util.Log;

import com.example.domi.myapplication2.R;

public class LoginActivity extends Activity {

    Button loginButton;
    EditText username, password;
    public DBLayer dblayer;
    public String session;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StrictMode.enableDefaults();

        //DB-Layer anlegen
        this.dblayer = new DBLayer(this);

        session = Files.GetSessionFile(this); //Läd die letzte session
        //Instanzvariablen initialisieren
        this.initInstances();

        //Überprüft ob der Benutzer sich berits eingeloggt hat
        Log.i("User", session);
        if(!session.contains("Gast") && session != ""){
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        //do nothing!

        //Benutzer darf Login-Activity nicht schließen, sonst könnte er auf die Rezepte ohne
        //einzuloggen zugreifen
    }

    private void initInstances(){
        //Instanzen aus XMl bekannt machen
        this.loginButton = (Button) findViewById(R.id.loginButton);
        this.username = (EditText) findViewById(R.id.usernameValue);
        this.password = (EditText) findViewById(R.id.passwordValue);
    }

    public void onLoginButtonClicked(View v){
        if(this.username.getText().toString().contains("Test")){
            this.finish();
        }
        else{
            String result= "";
            String username = this.username.getText().toString();
            String password = this.password.getText().toString();
            result = dblayer.getLoginData(username, password);

            if(result == "Databaseerror"){
                Toast.makeText(this, "Couldnt connect to database", Toast.LENGTH_LONG).show();
            }
            else if(result== "Wrong Login") {
                Toast.makeText(this, "Falscher Username oder falsches Passwort!", Toast.LENGTH_LONG).show();
            }
            else if(result == "Right Login") {
                Files.SetSessionFile(this, username);
                this.finish();
            }
            else if(result =="Error"){
                Toast.makeText(this, "Etwas ist schief gelaufen!!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onRegisterButtonClicked(View v){
        //starte RegisterActivity
        Intent in = new Intent(this, RegisterActivity.class);
        startActivity(in);
    }
}
