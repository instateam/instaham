package com.example.domi.myapplication2;

import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

//Die "UI-Klasse" eines Rezeptes, beinhaltet also die sichtbaren Informationen eines Rezeptes
public class ReceptUI extends Fragment {

    //====== Daten =======
    Recept recept = null;   //zugehöriges Rezept
    boolean receptLoaded = false;

    //====== Oberfläche ======
    TextView textViewTopic;         //Überschrift d. Rezeptes
    TextView textViewDifficulty;    //Schwierigkeit
    TextView textViewTimeEffort;    //Zeitaufwand
    TextView textViewAuthor;        //Autor
    TextView textViewReleaseDate;   //Veröffentlichungsdatum
    TextView textViewCategories;    //Kategorien
    TextView textViewHint;          //Kurzbeschreibung
    TextView textViewIngredients;   //Zutaten
    TextView textViewFullDescription;//Zubereitungsbeschreibung
    TextView textViewComments;      //Kommentare

    ImageView imageViewPicture;     //Bild

    RatingBar ratingBarRating;      //Bewertung




    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.recept_ui, container, false);
    }

    public ReceptUI() {
        // Required empty public constructor
    }
    public void addRecept(Recept rec){
        //füge ein Rezept zu diesem Fragment hinzu
        this.recept = rec;
        this.receptLoaded = true;

        //XML-Elemente bekanntmachen
        this.textViewTopic = (TextView) getView().findViewById(R.id.topic);
        this.textViewDifficulty = (TextView) getView().findViewById(R.id.difficulty);
        this.textViewTimeEffort = (TextView) getView().findViewById(R.id.time);
        this.textViewAuthor = (TextView) getView().findViewById(R.id.author);
        this.textViewReleaseDate = (TextView) getView().findViewById(R.id.releaseDate);
        this.textViewCategories = (TextView) getView().findViewById(R.id.categories);
        this.textViewHint = (TextView) getView().findViewById(R.id.hint);
        this.textViewIngredients = (TextView) getView().findViewById(R.id.ingredients);
        this.textViewFullDescription = (TextView) getView().findViewById(R.id.howto);
        this.textViewComments = (TextView) getView().findViewById(R.id.comments);
        this.imageViewPicture = (ImageView) getView().findViewById(R.id.image);
        this.ratingBarRating = (RatingBar) getView().findViewById(R.id.ratingBar);

        //Rezept auf Oberfläche anzeigen
        this.updateView();
    }

    public void updateView(){

        if (this.receptLoaded == true){
            //Informationen aus Recept laden
            this.textViewTopic.setText(this.recept.getTopic());

            //TODO:
            /*this.textViewDifficulty.setText(this..);
            this.textViewTimeEffort =
            this.textViewAuthor =
            this.textViewReleaseDate =
            this.textViewCategories =
            this.textViewHint =
            this.textViewIngredients =
            this.textViewFullDescription =
            this.textViewComments =
            this.imageViewPicture =
            this.ratingBarRating =*/
        }
        else {
            Log.e("ReceptUI", "Recept.class ist noch nicht bekannt. Rufe zuerst 'addRecept' auf!");
            return;
        }
    }

    public void addComment() {
        //füge Kommentar zu Rezept hinzu
    }

}
