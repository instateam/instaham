package com.example.domi.myapplication2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.database.Cursor;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;


public class AddRecept extends Activity implements AdapterView.OnItemSelectedListener {

    //instance variables
    EditText edittextTopicvalue, edittextIngredients, edittextFullDescription;
    Spinner spinnerDifficulty, spinnerTimeEffort, spinnerCategory;

    Uri fileUri;
    Uri selectedImage;
    Bitmap photo;
    String picturePath;
    String ba1;
    private boolean pictureisavalable = false;

    //Request Codes für Bild aufnehmen/von Handy laden
    static int RESULT_LOAD_IMAGE = 1;
    static int RESULT_OPEN_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addrecept);

        //XML Elemente holen
        this.edittextTopicvalue = (EditText) findViewById(R.id.edittextTopicValue);
        this.edittextIngredients = (EditText) findViewById(R.id.edittextIngredients);
        this.edittextFullDescription = (EditText) findViewById(R.id.edittextFullDescription);
        this.spinnerDifficulty = (Spinner) findViewById(R.id.spinnerDifficulty);
        this.spinnerTimeEffort = (Spinner) findViewById(R.id.spinnerTimeEffort);
        this.spinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);

        //Adapter für Spinnerelemente anlegen, zuständig für Informationen, die geladen werden sollen
        ArrayAdapter adapterDifficulty = ArrayAdapter.createFromResource(this, R.array.difficulty, android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter adapterTimeEffort = ArrayAdapter.createFromResource(this, R.array.timeEffort, android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter adapterCategory = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_dropdown_item);

        this.spinnerDifficulty.setAdapter(adapterDifficulty);
        this.spinnerTimeEffort.setAdapter(adapterTimeEffort);
        this.spinnerCategory.setAdapter(adapterCategory);

        this.spinnerTimeEffort.setOnItemSelectedListener(this);
        this.spinnerDifficulty.setOnItemSelectedListener(this);
        this.spinnerCategory.setOnItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.addrezept, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //hier: Unterscheidung von welchem Spinner es kommt!
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Bild von Kamera holen
        if (requestCode == RESULT_OPEN_CAMERA && resultCode == RESULT_OK) {

            selectedImage = data.getData();
            photo = (Bitmap) data.getExtras().get("data");

            //Cursor, der Bildpfad (URI) angibt
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ImageView imageView = (ImageView) findViewById(R.id.imageviewImage);
            imageView.setImageBitmap(photo);

            //Pictureflag, else it hase to load a 404 pictur
            pictureisavalable = true;
        }

        //Bild von Handybibliothek laden
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.imageviewImage);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

            //Pictureflag, else it hase to load a 404 pictur
            pictureisavalable = true;
        }
    }

    //Button-Methoden
    public void onButtonUploadClick(View v){

        if(this.edittextTopicvalue.getText().toString().equals("")){
            Toast.makeText(getApplication(), "Ein Titel fehlt!", Toast.LENGTH_LONG).show();
        }
        else if(this.edittextIngredients.getText().toString().equals("")){
            Toast.makeText(getApplication(), "Die Zutaten fehlen!", Toast.LENGTH_LONG).show();
        }
        else if(this.edittextFullDescription.getText().toString().equals("")){
            Toast.makeText(getApplication(), "Die vollständige Beschreibung fehlt!", Toast.LENGTH_LONG).show();
        }
        else{
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setMessage("Beschreibe dein Rezept in einem Satz: ");

            // Textfeld hinzufügen
            final EditText input = new EditText(this);
            alert.setView(input);

            AlertDialog.Builder builder = alert;
            builder.setPositiveButton("Hochladen", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if(input.getText().toString().equals("")){
                        Toast.makeText(getApplication(), "Eine Kurzbeschreibung fehlt!", Toast.LENGTH_LONG).show();
                    }
                    else{
                        // Image location URL
                        Log.e("path", "----------------" + picturePath);
                        // Image Configuration
                        Bitmap bm;
                        if (pictureisavalable == true) {
                            bm = BitmapFactory.decodeFile(picturePath);
                            ByteArrayOutputStream bao = new ByteArrayOutputStream();
                            bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                            byte[] ba = bao.toByteArray();
                            ba1 = Base64.encodeToString(ba, Base64.DEFAULT);

                        } else {
                            ba1 = "404";
                        }
                        //Set the File ready to upload by filling it with info
                        UploadToServer upload = new UploadToServer(AddRecept.this, edittextTopicvalue.getText().toString(), spinnerDifficulty.getSelectedItem().toString(),
                                spinnerTimeEffort.getSelectedItem().toString(), Files.GetSessionFile(AddRecept.this), spinnerCategory.getSelectedItem().toString(),
                                input.getText().toString(), edittextIngredients.getText().toString(), edittextFullDescription.getText().toString(), ba1);

                        //Uploads to the Server, Everything runs in Background
                        upload.execute();
                    }

            }
        }).setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled
            }
        });

        alert.show();
        }
    }

    //wenn User Bild von Kamera laden will
    public void onButtonCameraClick(View v) {
        //Kamera überprüfen
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            //Standardkamera öffnen
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            //Activity für Bildaufnahme starten
            startActivityForResult(intent, RESULT_OPEN_CAMERA);

        } else {
            Toast.makeText(getApplication(), "Kamera nicht unterstützt", Toast.LENGTH_LONG).show();
        }
    }

    //wenn User Bild vom Handy läd
    public void onButtonGalleryClick(View v){

        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


}
